---
id: best-practices
title: Recommendations or Best Practices
sidebar_label: Best practices
---

## Best Practices for Testing

By default, Elastic Path provides automated tests for various end to end flows of the React PWA Reference Storefront. You can run the tests directly on the development environment that is being used to implement the Reference Storefront. For configuring and executing tests, follow the instructions provided in the [README](https://github.com/elasticpath/react-pwa-reference-storefront/blob/master/README.md) file for the project.

When you create tests:
* Create additional automated tests when you add new functionality.
* Run automated tests to ensure that no regression is introduced after customizing the project.

## Best Practices for Extending React Reference Storefront
Elastic Path recommends following the best practices listed in this section. However, Elastic Path does not recommend any specific rule for creating a page or component.

* Base the storefront page as the actual page your shoppers visit in the storefront.
* Design a page to have a component corresponding to each functionality in the page. The functionality for a component, including references to the child components, are are configured within the component. You can customize components with properties and parameters to perform various actions, if required.
  - For example, a shopper can navigate to a category page to view product lists. This category page can have a component to display the products within the category, and that component can have another component for each of the products in the list. The component that displays the products in a category view can be used to display products on the search results page because of the similar functionality between the workflows.

## Global Product Attribute
* Use the formatted value of `store-name:category`, such as `vestri:Accessories`, to submit the product category information. Google Analytics handlers use the global product attribute `Tag`. You can set the global attribute for each product in the catalog to avoid submitting empty values for the product’s category to Google Analytics.

* Use [Elastic Path Dynamic Bundle Accelerator](https://code.elasticpath.com/accelerators/dynamic-bundles) for implementing Dynamic Bundles for products. For more information about the accelerator license, see the License and Copyright page(https://www.elasticpath.com/sites/default/files/elastic-license-and-copyright.pdf).
