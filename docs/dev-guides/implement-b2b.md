---
id: implement-b2b
title: Configuration and Implementation for B2B
sidebar_label: Implement B2B
---

## Account Management Service for REACT PWA Reference Storefront

The REACT PWA Reference Storefront uses the Account Management service to authenticate any B2B Commerce shopping flows.

For more information on the `b2b` configurations in the `src/ep.config.json` file, see: [README.md](https://github.com/elasticpath/react-pwa-reference-storefront/blob/master/README.md).

## Implementing Keycloak Theme

### Prerequisites

Ensure that the following parameters are configured as required:

- `cortexApi.scope`: The store or organization name used by the buyer organization divisions for your store.
- `cortexApi.pathForProxy`: The location of cortex instance used for any B2B Commerce shopping flows.

### About this task

By default, Keycloak provides a set of themes, such as login or accounts themes, that may be used for various actions that Keycloak supports. Use one of these existing themes for the supported flows or add a customized theme to the Keycloak Docker Image, if required. The REACT PWA Reference Storefront provides a login theme to use for the custom login page. For more information, see the [Elastic Path Account Management Deployment Guide](https://developers.elasticpath.com/accountmanagementdeployment/7.4.1/Elastic-Path-Account-Management-Deployment).

For the current KeyClock Docker Image, add the custom themes to the `/devops/docker/keycloak/themes` directory.

### Procedure

To apply a custom Keycloak theme to an Account Management instance:
1. Copy and extract the `https://github.com/elasticpath/react-pwa-reference-storefront/tree/master/eam/keycloak/themes.zip` file into the `/devops/docker/keycloak/themes` directory.
    - **Note:** Ensure that the new directory is in the following order: `/devops/docker/keycloak/themes/vestri/...`
2. Build and run the Account Management KeyClock Docker Image.
    - For more information, see the [Deploying and Configuring Keycloak](https://developers.elasticpath.com/accountmanagementdeployment/7.4.1/Elastic-Path-Account-Management-Deployment/Deploying-Account-Management-Service/Deploying-and-Configuring-Keycloak) section.
3. To use the newly added `vestri` custom theme, configure your client in the KeyClock administrative console.

## Related Documentation

- [Elastic Path Account Management Overview Guide](https://developers.elasticpath.com/accountmanagement/7.4.1/Elastic-Path-Account%C3%82%C2%A0-Management/Account-Management-Overview)
- [Elastic Path Account Management User Guide](https://developers.elasticpath.com/UG/7.4.1/logging-in-to-account-management-services)
-[Account Management API Documentation](https://developers.elasticpath.com/accountmanagementdeveloper/1.0.0/resource-reference-guide)
- [Elastic Path Account Management Deployment Guide](https://developers.elasticpath.com/accountmanagementdeployment/7.4.1/Elastic-Path-Account-Management-Deployment/Deploying-Account-Management-Service/Deploying-and-Configuring-Keycloak)
