---
id: ratings-reviews
title: PowerReviews Integration
sidebar_label: Ratings and Reviews
---

![Elasticpath Logo](assets/ElasticPath_vector_logo.png)
![plus](assets/plus.png)
![Zilker Logo](assets/Zilker_logo.png)
![plus](assets/plus.png)
![Power Reviews Logo](assets/power-reviews-logo-grey-text.png)

## Submitting Ratings and Reviews with PowerReviews

The integration with PowerReviews has been contributed to the REACT PWA Reference Storefront by [Zilker Technology](https://ztech.io/).

*"Decoupling both the development and the architecture as one of the main advantages of headless commerce platform has shown its true potential for developers while improving shopping experiences with PowerReviews."* - Zilker Technology

### About this task

The REACT PWA Reference Storefront uses the [PowerReviews](https://www.powerreviews.com/) service to get customer reviews, ratings, and queries.

The React PWA Reference Storefront is pre-configured with a custom component for PowerReviews. This component interacts with various widgets that PowerReviews displays. When PowerReviews adds new widgets, you can get the new widget by updating the component with the widget. For more information on updating components, see the [Creating New Components in React](dev-guides/extending.md) section.

This implementation makes use of PowerReviews feedless product catalog synchronization. By using this method, the product information updates every time the product display page is presented. For more information, see [Power Reviews Feedless Product Catalog ](https://help.powerreviews.com/Content/Product%20Catalog/Feedless.htm).

This section provides guidelines to improve the customer experience with PowerReviews.

### Procedure

1. Update the `ep.config.json` file with the following configuration settings provided by PowerReviews:

| **Parameter**| **Description**|
|--|--|
|`PowerReviews.enable`| Enable the integration component for PowerReviews. For more information, see [here](https://www.powerreviews.com/).|
|`PowerReviews.api_key`| The PowerReviews API Key used to connect the PowerReviews component.|
|`PowerReviews.merchant_group_id`| The PowerReviews Merchant Group Identifier used to connect the PowerReviews component.|
|`PowerReviews.merchant_id`| The PowerReviews Merchant Identifier used to connect the PowerReviews component.|

The PowerReviews javascript libraries and snippets have already been added to the product detail page and standalone review wrapper pages in the REACT PWA Reference Storefront to enable writing a review, and questions and answers.
* `src/components/powerreview.main.jsx`
* `src/containers/WriteReviewPage.jsx`

Properties presented in these snippets may be updated as required. For more information, see [PowerReviews JavaScript Reference Guide](https://help.powerreviews.com/Content/Platform/JavaScript%20Reference%20Guide.htm).

PowerReviews injects the initialization code on these two pages and places the corresponding containers in the appropriate places on the pages.

In the [PowerReviews Portal](https://auth.powerreviews.com/login) portal, you can view, filter, and export the statistics on
* Submitted ratings
* Review moderation
* Page Customizations
* Other solution customizations

After configuring the settings, the PowerReviews components are injected into the Product Details page:

![powerreviews_screenshot1](assets/powerreviews_screenshot1.png)

Upon configuration of the above settings, the PowerReviews components will be injected into the Write a Review and Question and Answers page:

![powerreviews_screenshot3](assets/powerreviews_screenshot2.png)

For more information on the customizations available to set using PowerReviews, [Log in to the page](https://auth.powerreviews.com/login) or see the [Help Center](https://help.powerreviews.com/Content/Home.htm).

## Related Documentation

- [Zilker Technology](https://ztech.io/)
- [PowerReviews](https://www.powerreviews.com/)
