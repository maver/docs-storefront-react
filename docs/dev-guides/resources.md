---
id: resources
title: Related Resources that may be helpful
sidebar_label: Related resources
---

## Elastic Path Commerce Resources

- [Elastic Path Developer Portal ](http://touchpoint-developers.elasticpath.com/)
- [Cortex API Front-End Documentation](https://developers.elasticpath.com/commerce/api-client-home)
- [Cortex API Developer Documentation](https://developers.elasticpath.com/commerce/api-server-home/)
- [Elastic Path Commerce Documentation](https://developers.elasticpath.com/commerce/core-home)

## References

The following resources provide more information on the third-party technologies that Elastic Path uses for developing React Reference Storefront:

* [React](https://reactjs.org/)
* [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
* [Webpack](https://webpack.js.org/)
* [Babel](https://babeljs.io/)
* [Workbox](https://developers.google.com/web/tools/workbox/)
