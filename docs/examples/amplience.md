---
id: amplience
title: Dynamic Media with Amplience
sidebar_label: Amplience Integration
---

## Content Management for Assets and Catalog Images Integration Example

The React PWA Reference Storefront is pre-configured for integration with various Content Management solutions by externalizing the content URLs through the storefront application configurations. The default URLs are configured to reference images, which are located on Amazon S3. However, you may use other CMS providers as you prefer.

Configuration properties for content URLs are defined as follows:
* `skuImagesUrl`: Path to catalog images hosted on an external CMS. Set this to the complete URL of your images, replacing the sku/file-name with the string `%sku%`. This value is populated when the page is loaded with the values retrieved by Cortex.
* `siteImagesUrl`: Path to site content and marketing images hosted on an external CMS. Set this to the complete URL of your images, replacing the file name and file extension with the string `%fileName%`. This value is populated when the page is loaded with the values retrieved by your components. If there is an issue with retrieving the values, the value is populated with assets locally available in `./src/images/site-images`.

If you use another CMS, you must update the configurations to reflect the public URLs of the content being retrieved by the particular content provider.

### External Content Management System Integration Example

**Amplience Content Hub Integration**

Amplience Content Hub is a cloud-based Content Management System (CMS) that provides services, such as dynamic image rendering.

You can upload catalog images and site content images to Amplience using the Content Hub. After uploading the images, provide the content delivery URL with the appropriate `sku/file` name placeholders in the Reference Storefront's configuration for both `skuImagesUrl` and `siteImagesUrl`. For more information, see [Configuration Parameter Descriptions](https://github.com/elasticpath/react-pwa-reference-storefront#configuration-parameter-descriptions).
The following example shows the fields populated based on the tag of the Amplience account and the directory structure of the assets:

```json
{
  "skuImagesUrl": "https://i1.adis.ws/i/<tag>/%sku%",
  "siteImagesUrl": "https://i1.adis.ws/i/<tag>/%fileName%",
}
```


## Related Documentation

* [Amplience](https://amplience.com/)
* [Amplience Content Hub](https://amplience.com/content-hub)
