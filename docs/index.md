---
id: index
title: Overview of React PWA Reference Storefront
sidebar_label: Overview
---

## Introduction

The React PWA Reference Storefront is a flexible e-commerce website built on Elastic Path’s [RESTful](http://en.wikipedia.org/wiki/Representational_state_transfer) e-commerce API,  [Cortex API](https://developers.elasticpath.com/commerce/7.3/Cortex-API-Front-End-Development/Getting-Started/Introduction). Through Cortex API, the storefront get to use the e-commerce capabilities provided by Elastic Path Commerce and get data in a RESTful manner.

React Reference Storefront is composed of containers (pages) and React components. Each page in the storefront is composed of various components that fulfills the purpose of the page. For example, home, cart, categories, checkout, and registration are separate pages. Each page consists of various components, such as navigation, footer, product list, or products.

The Storefront is designed as an open source mobile Progressive Web Application (PWA) that has the capabilities for local browser storage page caching and persistent session management. This PWA is built using the ['React.js'](https://reactjs.org/), [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/), and [Babel](https://babeljs.io/) technologies. [Webpack](https://webpack.js.org/) and [node.js](https://nodejs.org/en/) enable the application layer interactions through configurable web server.

With theming, you can change the presentation of the React Reference Storefront without modifying JavaScript. React Reference Storefront uses the dynamic stylesheet language, [less](http://lesscss.org/), to changes the themes. You can change the look and feel of the storefront by modifying the corresponding `{less}` files.

Each theme is a set of `{less}` files, which can be compiled into a `style.css` file. The `{less}` files for each theme are organized in the same order as the storefront is presented.
For example, `CartPage.less` contains CSS for the cart’s container page, and  `cart.main.less` contains CSS for the main component in the cart page.

## Features

In React Reference Storefront, e-commerce functionality, such as cart, authentication, profile, or search, and the website presentation are separated. The front-end developers need not change JavaScript to update the CSS files to change the presentation and the JavaScript developers can develop functionality without changing front-end. Each customization layer is separated into it's own layer from the core code, so developers need not change the Storefront’s utility engine.

You can access the React Reference Storefront on various devices, such as tablets, mobile phones, or computers.

![Feature Guide](assets/featureSupport.png)

## Platform Architecture

The React PWA Reference Storefront is composed of layers that work together to provide various capabilities to operate the storefront. The following diagram shows the storefront architecture: 

![Reference Store Architecture](assets/Ref_Store_Architecture.png)

* Presentation Layer: Provides the user interface management capabilities using a modern UI framework. The configuration files in this layer are:
    * `index.html` - The front-end html document that is populated by the React pages and components.
    * `App.js` - The javascript file that contains the import instructions for the React components and the subsequent layers. When this script is run, all subsequent components are loaded on the page.
    * `index.js` - The javascript file that contains the import instructions for the required frameworks, such as bootstrap, React, and renders the `App.js` file. This file also registers the PWA service worker.
    * CSS - The file that consists of the custom stylesheets and static page assets for the storefront front-end presentation.
* Route/Page Layer: Routes browser requests to the subsequent pages. This layer consists of:
    *  `routes.js` - The javascript file that contains routes and mappings to the `.jsx` React pages.
    *  Pages - The top-level pages that loads the required components for each page.
    *  Service worker - A service worker created using Workbox to manage caching and PWA capabilities.
* Component Layer: Provides the components to be loaded within each page. The components are created depending on the functionality of the item that the pages interact with or loads. For example, the component layer provides the login component for the home page.
* Web Server Layer: Enables the interaction between the application and the service layer. 
* API Layer: The application layer of React PWA Reference Storefront is built on [Cortex API](https://developers.elasticpath.com/commerce/7.3/Cortex-API-Front-End-Development/Getting-Started/Introduction). Through the Cortex API, the storefront get to use the e-commerce capabilities provided by Elastic Path Commerce. PWA interacts with the required services through this layer.

### Cortex Integration
The storefront invokes Cortex APIs to ensure that the best practices of consuming hypermedia APIs are followed.

![Cortex](assets/cortex-page-diagram.png)

By modifying the following files, you can customize the API calls that make the REST HTTP request:
* The `Cortex.js` file contains the methods and classes that fetch data for the API calls made to Cortex. You can customize this file with additional headers or generic request modifications in the `cortexFetch()` function for custom implementations. This file also includes `x-headers` and the ability to add any additional headers. 
* The `AuthService.js` contains the authentication functions used to authenticate users with Cortex using OAuth2. The OAuth tokens are saved in the local storage of the web application. For authenticating subsequent Cortex requests, the tokens are retrieved and added into the header.

You can create zoom queries by populating an array with all required queries and appending it to the request to Cortex. When you create a zoom query, request only the additional data needed by the component using the query. Elastic Path recommends to avoid requesting unnecessary data, and circular zooms for better performance.

### Workflow
1. The top-level root for the REST call is retrieved from Cortex. This URL consists of related links that provides URLs to the children of the root.
2. After retrieving the root level, subsequent actions are performed against Cortex on the children of the root, depending on the action intended to be performed by the React Component initiating the REST request.

> **Note**: For the subsequent calls, the storefront must call only the URL that Cortex returns when you call the root. You must not create custom URL in the storefront component settings to ensure that any change in the API names do not change the URL. Use the `rels` in Cortex response to make subsequent calls. However, for the OAuth implementation, you might want to create a URL in the storefront.

