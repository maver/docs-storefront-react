---
id: requirements
title: Requirements and Specifications
sidebar_label: Requirements
---

## Development tools

For installing and customizing React Reference Storefront, in addition to a valid Elastic Path development environment, the following software are required:

- [Git](https://git-scm.com/downloads)
- [Node.js](https://nodejs.org/en/download/)
- [Visual Studio Code](https://code.visualstudio.com/) with the following extensions:
	- [Debugger for Chrome](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome)
	- [ESLint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

## Knowledge Requirements

For extending and customizing the storefront, knowledge in the following technologies are required:

* [React](https://reactjs.org/)
* [jQuery](https://jquery.com/)
* [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
* [CSS](https://en.wikipedia.org/wiki/Cascading_Style_Sheets)
* [less](http://lesscss.org/)

## Supported Platforms

Elastic Path recommends using certified platforms, which are used for the regression and performance testing, for the product configuration. Elastic Path products might function correctly when deployed with compatible platforms, however, these platforms are not tested with the products. Elastic Path does not provide recommendations or best practices for these technologies.

### Browsers
- Compatible with:
	* Mozilla Firefox
- Certified:
	* Internet Explorer (10 or later)
	* Google Chrome
	* Safari

### Devices
- Compatible with:
	* Android phones
	* iOS phones
- Certified:
    * Android tablets (10 and 7 inch)
	* iOS tablets (10 and 7 inch)

## Technology Stack

The React Reference Storefront technologies are robust and extensible. With these technologies, JavaScript developers and the front-end developers can customize storefront quickly with ease.

|  Technology| Description|Version|Domain|
|--|--|--|--|
| [**React.js**](https://reactjs.org/) |The JavaScript library for building a user interface using the components for single page applications.|16.4.2| Development |
|[**Webpack**](https://webpack.js.org/)| An open-source JavaScript module bundler. Webpack takes modules with dependencies and generates static assets for the modules. |4.16.0|Development |
|  [**jQuery**](https://jquery.com/) | The JavaScript library used for the base DOM abstraction layer. | 3.3.1 |Development |
| [**Babel**](https://babeljs.io/) |The Javascript compiler. | 7.0.0 |Development |
| [**Bootstrap.js**](https://getbootstrap.com/docs/4.0/getting-started/introduction/) | A free and open-source front-end framework for designing websites and web applications. |  4.4.1|Development |
|[**node.js**](https://nodejs.org/en/)|An open-source, cross-platform JavaScript run-time environment that executes JavaScript code server-side.|8.11.2|Development |
|[**Workbox**](https://developers.google.com/web/tools/workbox/)|The JavaScript libraries for adding offline support to web applications|3.4.1|Development |
|[**Puppeteer**](https://developers.google.com/web/tools/puppeteer/)|The framework for testing web applications using Chrome automation.|1.12.2|QA|
