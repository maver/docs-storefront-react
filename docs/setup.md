---
id: setup
title: Setup your development environment
sidebar_label: Setup
---

To start building your storefront:

```bash
# Clone the Git repository
git clone https://github.com/elasticpath/react-pwa-reference-storefront.git

# Go into the cloned directory
cd react-pwa-reference-storefront

# Install the necessary dependencies
npm install

# Configure the ./src/ep.config.json file

# Start the server in development mode
npm start
```

You will also need an elastic path platform which you will be used as the backend of this storefront.

## Running React Reference Storefront

1. Run the required applications on the corresponding ports:

|  Application| Port|
|--|--|
|Cortex API| 9080|
|Search Server| 8082|
|Webpack Store Server|8080|
|Commerce Manager|8081|

For more information on installing the React Reference Storefront, see the [Project Readme](https://github.com/elasticpath/react-pwa-reference-storefront/blob/master/README.md) file.
