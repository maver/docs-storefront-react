// See https://docusaurus.io/docs/site-config for all the possible site configuration options.

const projectRepo = "https://github.com/elasticpath/react-pwa-reference-storefront";
const docRepo = "";

const siteConfig = {
  title: 'React PWA Storefront', // Title for your website.
  tagline: 'Documentation for React based storefront',
  url: 'https://documentation.elasticpath.com', // Your website URL
  baseUrl: '/storefront-react/', // Base URL for your project */
  // For github.io type URLs, you would set the url and baseUrl like:
  //   url: 'https://facebook.github.io',
  //   baseUrl: '/test-site/',

  // Used for publishing and more
  projectName: 'docs-storefront-react',
  organizationName: 'elasticpath',
  // For top-level user or org sites, the organization is still the same.
  // e.g., for the https://JoelMarcey.github.io site, it would be set like...
  //   organizationName: 'JoelMarcey'

  // For no header links in the top nav bar -> headerLinks: [],
  headerLinks: [
    { doc: 'index', label: 'Documentation' },
    { href: projectRepo, label: 'Github', external: true },
    { blog: false },
    { search: true },
  ],

  //Algolia search configuration
  algolia: {
    apiKey: '0c9c7c9ad402f61c9c3f0c74bb2452d1',
    indexName: 'elasticpath_react_pwa',
    placeholder: 'Search docs',
  },

  /* path to images for header/footer */
  headerIcon: 'img/logo-elasticpath-tm.png',
  footerIcon: 'img/ep-192x192.png',
  favicon: 'img/favicon/ep.ico',

  /* Colors for website */
  colors: {
    primaryColor: '#0178c2',
    secondaryColor: '#00A3DD',
  },

  // This copyright info is used in /core/Footer.js and blog RSS/Atom feeds.
  copyright: `Copyright © ${new Date().getFullYear()} Elastic Path, Inc.`,

  highlight: {
    // Highlight.js theme to use for syntax highlighting in code blocks.
    theme: 'default',
  },

  // Add custom scripts here that would be placed in <script> tags.
  scripts: ['https://buttons.github.io/buttons.js'],

  // On page navigation for the current documentation page.
  onPageNav: 'separate',
  // No .html extensions for paths.
  // cleanUrl: true,

  // Open Graph and Twitter card images.
  ogImage: 'img/ep-192x192.png',
  twitterImage: 'img/ep-192x192.png',
  twitterUsername: 'elasticpath',

  // Show documentation's last contributor's name.
  // enableUpdateBy: true,

  // Show documentation's last update time.
  enableUpdateTime: true,

  //Enable scrolling to top button
  scrollToTop: true,

  //users to display on landing page
  users: [
    { infoLink: 'https://ztech.io/', image: '/storefront-react/docs/assets/Zilker_logo.png', caption: 'Zilker Technology' },
    { infoLink: 'https://www.powerreviews.com/', image: '/storefront-react/docs/assets/power-reviews-logo-grey-text.png', caption: 'Power Reviews' },
    { infoLink: 'https://amplience.com/', image: '/storefront-react/docs/assets/amplience_logo.png', caption: 'Amplience' },
    { infoLink: 'https://indi.com/', image: '/storefront-react/docs/assets/indi_logo.png', caption: 'Indi' }
  ],

  // You may provide arbitrary config keys to be used as needed by your
  // template. For example, if you need your repo's URL...
  //   repoUrl: 'https://github.com/facebook/test-site',
};

module.exports = siteConfig;
